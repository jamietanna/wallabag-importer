require 'sqlite3'
require 'net/http'
require 'json'

module Wallabag
  class Wallabag

    attr_reader :username
    attr_reader :client_id
    attr_reader :client_secret
    attr_reader :base_url
    attr_reader :sqlite_path

    def initialize(username, password, client_id, client_secret, base_url, sqlite_path)
      [
        {
          'Username' => username
        },
        {
          'Password' => password
        },
        {
          'Client ID' => client_id
        },
        {
          'Client Secret' => client_secret
        },
        {
          'Base URL' => base_url
        },
        {
          'SQLite Path' => sqlite_path
        }
      ].each do |entry|
        entry.each do |k, v|
          raise "#{k} cannot be empty" if v.to_s.empty?
        end
      end

      uri = URI.parse(base_url)
      raise 'Base URL must be HTTP/HTTPS URI' unless  'http'  == uri.scheme || \
                                                      'https' == uri.scheme

      @client_id = client_id
      @client_secret = client_secret
      @base_url = base_url
      @sqlite_path = sqlite_path
      @username = username
      @password = password
    end

    def get_posts
      db = SQLite3::Database.new @sqlite_path
      posts = []
      db.execute('SELECT URL, FAVORITE, ARCHIVE from ARTICLE;') do |post|
        posts << {
          url: post[0],
          starred: 1 == post[1],
          archived: 1 == post[2]
        }
      end
      posts
    end

    def get_access_token
      [
        {
          'Username' => @username
        },
        {
          'Password' => @password
        }
      ].each do |entry|
        entry.each do |k, v|
          raise "#{k} cannot be empty" if v.to_s.empty?
        end
      end

      headers = {
        'Content-Type' => 'application/json'
      }
      body = {
        'grant_type' => 'password',
        'client_id' => @client_id,
        'client_secret' => @client_secret,
        'username' => username,
        'password' => password
      }

      uri = URI.parse(@base_url + '/oauth/v2/token')
      http = Net::HTTP.new(uri.host, uri.port)
      request = Net::HTTP::Post.new(uri.request_uri, headers)
      request.body = body.to_json
      response = http.request(request)
      body = JSON.parse(response.body)

      case response.code
      when '200'
        body['access_token']
      when '400'
        raise "Bad Request: #{body['error_description']}"
      end
    end

    def post_article(article, access_token)
      headers = {
        'Authorization' => "Bearer #{access_token}"
      }
      form_body = "url=#{article[:url]}"\
                  "&starred=#{article[:starred] ? 1 : 0}"\
                  "&archive=#{article[:archived] ? 1 : 0}"

      uri = URI.parse(@base_url + '/api/entries.json')
      http = Net::HTTP.new(uri.host, uri.port)
      request = Net::HTTP::Post.new(uri.request_uri, headers)
      request.body = form_body
      response = http.request(request)
      body = JSON.parse(response.body)
      body['id'] if '200' == response.code
    end

    def post_articles(articles)
      access_token = get_access_token
      ret = []
      articles.each do |article|
        ret << post_article(article, access_token)
      end
      ret
    end

    private
    attr_reader :password

  end
end
