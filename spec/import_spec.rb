require_relative '../import'

describe Wallabag::Wallabag do
  let(:subject) { Wallabag::Wallabag.new('uname', 'securepassword', 'id', 'secret_thing', 'http://base_url', '/path/to/local_sqlite') }
  include Wallabag

  context '.initialize' do
    it 'sets initial state' do
      sut = Wallabag::Wallabag.new('username', 'password', 'id', 'secret_thing', 'http://base_url', '/path/to/local_sqlite')
      expect(sut.client_id).to eq 'id'
      expect(sut.client_secret).to eq 'secret_thing'
      expect(sut.base_url).to eq 'http://base_url'
      expect(sut.sqlite_path).to eq '/path/to/local_sqlite'
      expect(sut.username).to eq 'username'
    end

    it 'errors if username empty' do
      expect { Wallabag::Wallabag.new(nil, 'password', 'id', 'secret_thing', 'http://base_url', '/path/to/local_sqlite') }.to raise_error('Username cannot be empty')
      expect { Wallabag::Wallabag.new('', 'password', 'id', 'secret_thing', 'http://base_url', '/path/to/local_sqlite') }.to raise_error('Username cannot be empty')
    end

    it 'errors if client_secret empty' do
      expect { Wallabag::Wallabag.new('username', nil, 'id', nil, 'http://base_url', '/path/to/local_sqlite') }.to raise_error('Password cannot be empty')
      expect { Wallabag::Wallabag.new('username', '', 'id', '', 'http://base_url', '/path/to/local_sqlite') }.to raise_error('Password cannot be empty')
    end

    it 'errors if client_id empty' do
      expect { Wallabag::Wallabag.new('username', 'password', nil, 'secret_thing', 'http://base_url', '/path/to/local_sqlite') }.to raise_error('Client ID cannot be empty')
      expect { Wallabag::Wallabag.new('username', 'password', '', 'secret_thing', 'http://base_url', '/path/to/local_sqlite') }.to raise_error('Client ID cannot be empty')
    end

    it 'errors if client_secret empty' do
      expect { Wallabag::Wallabag.new('username', 'password', 'id', nil, 'http://base_url', '/path/to/local_sqlite') }.to raise_error('Client Secret cannot be empty')
      expect { Wallabag::Wallabag.new('username', 'password', 'id', '', 'http://base_url', '/path/to/local_sqlite') }.to raise_error('Client Secret cannot be empty')
    end

    it 'errors if base_url empty' do
      expect { Wallabag::Wallabag.new('username', 'password', 'id', 'secret_thing', nil, '/path/to/local_sqlite') }.to raise_error('Base URL cannot be empty')
      expect { Wallabag::Wallabag.new('username', 'password', 'id', 'secret_thing', '', '/path/to/local_sqlite') }.to raise_error('Base URL cannot be empty')
      expect { Wallabag::Wallabag.new('username', 'password', 'id', 'secret_thing', 'file://test', '/path') }.to raise_error('Base URL must be HTTP/HTTPS URI')
    end

    it 'errors if sqlite_path empty' do
      expect { Wallabag::Wallabag.new('username', 'password', 'id', 'secret_thing', 'http://base_url', nil) }.to raise_error('SQLite Path cannot be empty')
      expect { Wallabag::Wallabag.new('username', 'password', 'id', 'secret_thing', 'http://base_url', '') }.to raise_error('SQLite Path cannot be empty')
    end

  end

  context 'get_posts' do
    it 'returns [] if no entries' do
      db = spy('SQLite3::Database')
      expect(SQLite3::Database).to receive(:new)
        .with('/path/to/local_sqlite')
        .and_return(db)
      # not yield anything
      expect(db).to receive(:execute)
        .with('SELECT URL, FAVORITE, ARCHIVE from ARTICLE;')

      expect(subject.get_posts).to eq([])
    end

    it 'returns singleton if single entries' do
      db = spy('SQLite3::Database')
      expect(SQLite3::Database).to receive(:new)
        .with('/path/to/local_sqlite')
        .and_return(db)
      expect(db).to receive(:execute)
        .with('SELECT URL, FAVORITE, ARCHIVE from ARTICLE;')
        .and_yield(['https://google.com', 0, 0])

      expected = [
        {
          url: 'https://google.com',
          starred: false,
          archived: false
        }
      ]
      expect(subject.get_posts).to match_array(expected)
    end

    it 'returns many if many entries' do
      db = spy('SQLite3::Database')
      expect(SQLite3::Database).to receive(:new)
        .with('/path/to/local_sqlite')
        .and_return(db)
      # SQLite's bool 1===true https://stackoverflow.com/a/9075991
      expect(db).to receive(:execute)
        .with('SELECT URL, FAVORITE, ARCHIVE from ARTICLE;')
        .and_yield(['https://google.com', 0, 0])
        .and_yield(['https://wallabag.org', 0, 1])
        .and_yield(['https://gitlab', 1, 0])

      expected = [
        {
          url: 'https://google.com',
          starred: false,
          archived: false
        },
        {
          url: 'https://wallabag.org',
          starred: false,
          archived: true
        },
        {
          url: 'https://gitlab',
          starred: true,
          archived: false
        }
      ]

      expect(subject.get_posts).to match_array(expected)
    end
  end

  context 'get_access_token' do

    # TODO: validate URL
    it 'returns access_token when successful' do
      subject = Wallabag::Wallabag.new('uname', 'password_here', 'id', 'secret_thing', 'http://base_url', '/path/to/local_sqlite')
      http = double('http')
      expect(Net::HTTP).to receive(:new).with('base_url', 80)
        .and_return(http)

      headers = {
        'Content-Type' => 'application/json'
      }
      body = {
        'grant_type' => 'password',
        'client_id' => 'id',
        'client_secret' => 'secret_thing',
        'username' => 'uname',
        'password' => 'password_here'
      }

      request = double('request')
      expect(Net::HTTP::Post).to receive(:new).with('/oauth/v2/token', headers)
        .and_return(request)
      expect(request).to receive(:body=).with(body.to_json)

      response = double('response')
      expect(http).to receive(:request).with(request)
        .and_return(response)

      expect(response).to receive(:code).and_return('200')

      response_body = {
        'access_token' => 'wibble'
      }
      expect(response).to receive(:body)
        .and_return(response_body.to_json)

      expect(JSON).to receive(:parse)
        .with(response_body.to_json)
        .and_return(response_body)

      expect(subject.get_access_token).to eq 'wibble'
    end

    it 'raises an error when invalid credentials are provided' do
      expect(subject).to receive(:username).and_return 'wibble'
      expect(subject).to receive(:password).and_return 'passwibble'
      uri = double('uri')
      expect(URI).to receive(:parse).with('http://base_url/oauth/v2/token')
        .and_return(uri)
      expect(uri).to receive(:host).and_return('base_url')
      expect(uri).to receive(:port).and_return('80')
      expect(uri).to receive(:request_uri).and_return('/oauth/v2/token')

      http = double('http')
      expect(Net::HTTP).to receive(:new).with('base_url', '80')
        .and_return(http)

      headers = {
        'Content-Type' => 'application/json'
      }
      body = {
        'grant_type' => 'password',
        'client_id' => 'id',
        'client_secret' => 'secret_thing',
        'username' => 'wibble',
        'password' => 'passwibble'
      }

      request = double('request')
      expect(Net::HTTP::Post).to receive(:new).with('/oauth/v2/token', headers)
        .and_return(request)
      expect(request).to receive(:body=).with(body.to_json)

      response = double('response')
      expect(http).to receive(:request).with(request)
        .and_return(response)

      expect(response).to receive(:code).and_return('400')

      response_body = {
        'error' => 'invalid_grant',
        'error_description' => 'Fake error here'
      }
      expect(response).to receive(:body)
        .and_return(response_body.to_json)

      expect(JSON).to receive(:parse)
        .with(response_body.to_json)
        .and_return(response_body)

      expect { subject.get_access_token }.to\
        raise_error('Bad Request: Fake error here')
    end
  end

  context 'post_articles' do
    it 'delegates to post_article' do
      expect(subject).to receive(:get_access_token).and_return 'wobble'
      article_google = {
        url: 'https://google.com',
        starred: false,
        archived: false
      }
      article_wallabag = {
        url: 'https://wallabag.org',
        starred: false,
        archived: true
      }
      article_gitlab = {
        url: 'https://gitlab',
        starred: true,
        archived: false
      }
      articles = [article_google, article_wallabag, article_gitlab]
      expect(subject).to receive(:post_article).with(article_google, 'wobble')
        .and_return 4
      expect(subject).to receive(:post_article).with(article_wallabag, 'wobble')
        .and_return 7
      expect(subject).to receive(:post_article).with(article_gitlab, 'wobble')
        .and_return 14


      expected = [4, 7, 14]
      expect(subject.post_articles(articles)).to eq expected
    end
  end

  context 'post_article' do
    it 'returns the ID from wallabag' do
      http = double('http')
      expect(Net::HTTP).to receive(:new).with('base_url', 80)
        .and_return(http)
      headers = {
        'Authorization' => 'Bearer wibble'
      }
      form_body = 'url=https://wallabag.org&starred=0&archive=0'

      request = double('request')
      expect(request).to receive(:body=).with(form_body)

      expect(Net::HTTP::Post).to receive(:new).with('/api/entries.json', headers)
        .and_return request
      response = double('response')
      expect(http).to receive(:request).with(request)
        .and_return response

      response_body = {
        'url' => 'https://wallabag.org',
        'id' => 123
      }
      expect(response).to receive(:code).and_return '200'
      expect(response).to receive(:body).and_return response_body.to_json

      expect(JSON).to receive(:parse)
        .with(response_body.to_json)
        .and_return(response_body)


      article_wallabag = {
        url: 'https://wallabag.org',
        starred: false,
        archived: false
      }
      expect(subject.post_article(article_wallabag, 'wibble')).to eq 123
    end

    it 'sets the status of starred' do
      http = double('http')
      expect(Net::HTTP).to receive(:new).with('base_url', 80)
        .and_return(http)
      headers = {
        'Authorization' => 'Bearer wibble'
      }
      form_body = 'url=http://google.com&starred=1&archive=0'

      request = double('request')
      expect(request).to receive(:body=).with(form_body)

      expect(Net::HTTP::Post).to receive(:new).with('/api/entries.json', headers)
        .and_return request
      response = double('response')
      expect(http).to receive(:request).with(request)
        .and_return response

      response_body = {
        'url' => 'http://google.com',
        'id' => 123
      }
      expect(response).to receive(:code).and_return '200'
      expect(response).to receive(:body).and_return response_body.to_json

      expect(JSON).to receive(:parse)
        .with(response_body.to_json)
        .and_return(response_body)


      article_google = {
        url: 'http://google.com',
        starred: true,
        archived: false
      }
      expect(subject.post_article(article_google, 'wibble')).to eq 123
    end


    it 'sets the status of archived' do
      http = double('http')
      expect(Net::HTTP).to receive(:new).with('base_url', 80)
        .and_return(http)
      headers = {
        'Authorization' => 'Bearer wibble'
      }
      form_body = 'url=http://google.com&starred=0&archive=1'

      request = double('request')
      expect(request).to receive(:body=).with(form_body)

      expect(Net::HTTP::Post).to receive(:new).with('/api/entries.json', headers)
        .and_return request
      response = double('response')
      expect(http).to receive(:request).with(request)
        .and_return response

      response_body = {
        'url' => 'http://google.com',
        'id' => 123
      }
      expect(response).to receive(:code).and_return '200'
      expect(response).to receive(:body).and_return response_body.to_json

      expect(JSON).to receive(:parse)
        .with(response_body.to_json)
        .and_return(response_body)


      article_google = {
        url: 'http://google.com',
        starred: false,
        archived: true
      }
      expect(subject.post_article(article_google, 'wibble')).to eq 123
    end

    pending 'error conditions'
  end
end
